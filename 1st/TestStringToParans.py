import unittest
from StringToParans import ConvertToParans


class TestToParans(unittest.TestCase):
    def test_unique(self):
        self.assertEqual(ConvertToParans("din"), "(((")

    def test_repeat_pattern(self):
        self.assertEqual(ConvertToParans("recede"), "()()()")

    def test_semi_repeat_pattern(self):
        self.assertEqual(ConvertToParans("Success"), ")())())")

    def test_symbols(self):
        self.assertEqual(ConvertToParans("(( @"), "))((")


if __name__ == "__main__":
    unittest.main()
