from collections import Counter


def ConvertToParans(string: str) -> str:
    lower_str = string.lower()
    dict_alpha = Counter(lower_str)
    parans = ["(" if dict_alpha.get(ch) == 1 else ")" for ch in lower_str]
    return "".join(parans)
