from StringToParans import ConvertToParans


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Usage:", sys.argv[0], "string to convert")
        sys.exit()
    result = ConvertToParans(sys.argv[1])
    print(result)
